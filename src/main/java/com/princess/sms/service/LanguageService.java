package com.princess.sms.service;

import com.princess.sms.model.LanguageList;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public interface LanguageService {


  /**
   * Method findSupportedLanguages.
   * 
   * @return LanguageList
   */
  public LanguageList findSupportedLanguages();

}
