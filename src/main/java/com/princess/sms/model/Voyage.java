package com.princess.sms.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@XmlRootElement(name = "voyage")
public class Voyage extends AbstractBaseObject {

  /**
   * 
   */
  private static final long serialVersionUID = -1628927891772473964L;
  private Ship ship;
  private String voyageNumber;
  private Date startDate;
  private Date endDate;

  /**
   * Method getShip.
   * 
   * 
   * 
   * @return Ship
   */
  public Ship getShip() {
    return ship;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  /**
   * Method getVoyageNumber.
   * 
   * 
   * 
   * @return String
   */
  public String getVoyageNumber() {
    return voyageNumber;
  }

  /**
   * Method setShip.
   * 
   * @param ship Ship
   */
  public void setShip(Ship ship) {
    this.ship = ship;
  }

  /**
   * Method setVoyageNumber.
   * 
   * @param voyageNumber String
   */
  public void setVoyageNumber(String voyageNumber) {
    this.voyageNumber = voyageNumber;
  }

  /**
   * Method toString.
   * 
   * 
   * 
   * @return String
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("ship", this.ship)
        .append("voyageNumber", this.voyageNumber).toString();
  }

  /**
   * Method equals.
   * 
   * @param object Object
   * @return boolean
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Voyage)) {
      return false;
    }
    Voyage rhs = (Voyage) object;
    return new EqualsBuilder().append(this.ship, rhs.ship).append(this.voyageNumber, rhs.voyageNumber).isEquals();
  }

  /**
   * Method hashCode.
   * 
   * @return int
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(ship).append(voyageNumber).toHashCode();
  }
}
